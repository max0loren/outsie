## Outsie build info

As the circuit is quite simple, not much can go wrong here.

Most components can be sourced from classic suppliers. Knobs and pots can be sourced from Thonk.

It needs a dual-gang audio (log) potentiometer.

The three 1/4" (6.35mm) jacks are not the most pretty, but their height is perfect with Thonk's right angled pots.

It needs a bit of wiring; mostly LEDs (which can be omitted safely if necessary)
and the headphones volume control potentiometer. See the wiring diagram on how to do it.
