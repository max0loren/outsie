## Outsie
**_A Loudest Warning format simple stereo interface._**

As the title says, it is a simple interface to output audio from a synthesizer, in only 1" (or Nominal Module Width).
It can drive headphones. It is stereo, but more like two different channels. It should (could) be processed further with a simple mixing desk, I guess.
It is based on Befaco Output V3 and Douglas Self _Small Signal Audio Design_, so it is rather simple.

### Module
This module has three sections:
- The top section are two outputs, with their volume control. They may be sent to a mixing desk.
- The middle sections contains the two inputs with visual feedback (LEDs).
- The bottom section contains the headphones driver, with its own volume potentiometer.

### This repository contains:
- The hardware files
    - The whole KiCad project
    - The Bill of Materials (BoM)
    - The gerber files
    - A pdf export of the schematics (readable online!)
- Physical description
    - The SVG for the panel design
    - The exported pdf


![alt text](module.jpg "built module")
